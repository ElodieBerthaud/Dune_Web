import initialState from "../../initialState";
import { GET_INVOICES_SUCCESS } from "../../../actions/actionTypes";

export default function getInvoices(state = initialState.invoices, action) {
  if (action.type === GET_INVOICES_SUCCESS) {
    return { ...state, invoices: action.invoices };
  } else {
    return state;
  }
}
