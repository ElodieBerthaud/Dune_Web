import initialState from "../../initialState";
import { SHOW_DASH_TMP_STOP } from "../../../actions/actionTypes";

export default function showNotif(state = initialState.showDashTmp, action) {
  if (action.type === SHOW_DASH_TMP_STOP) {
    return { ...state, showDash: false };
  } else {
    return state;
  }
}
