import initialState from "../../initialState";
import { NOTIFS_DASH_STOP } from "../../../actions/actionTypes";

export default function showNotif(state = initialState.showDash, action) {
  if (action.type === NOTIFS_DASH_STOP) {
    return { ...state, showDash: false };
  } else {
    return state;
  }
}
