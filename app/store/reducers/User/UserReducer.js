import initialState from "../../initialState";
import { GET_USER_INFOS } from "../../../actions/actionTypes";

export default function login(state = initialState.user, action) {
  if (action.type === GET_USER_INFOS) {
    return {
      ...state,
      lastname: action.lastname,
      name: action.name,
      email: action.email,
      pic: action.pic
    };
  } else {
    return state;
  }
}
