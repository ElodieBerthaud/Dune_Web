import { put, call } from "redux-saga/effects";
import {
  get_sub_info_api,
  subscribe_api
} from "../../Api/Subs/subs_api_functions";

// Get all student of a user
export function* get_sub_infos(datas) {
  try {
    const response = yield call(get_sub_info_api, datas);

    if (response.status === 200) {
      yield put({
        type: "GET_SUB_INFO_SUCCESS",
        current_abo: response.data.response[0].id_abonnement,
        isValid: response.data.response[0].status,
        current_abo_state: {
          apps_left: response.data.response[0].apps_left,
          storage_left: response.data.response[0].storage_total,
          apps_total: response.data.response[0].apps_total,
          storage_total: response.data.response[0].storage_total
        }
      });
    } else {
      yield put({ type: GET_SUB_INFO_ERROR });
    }
  } catch (e) {}
}

// Get all student of a user
export function* subscribe(datas) {
  yield put({ type: "LOADING", loadmessage: "Veuillez patienter." });
  try {
    const response = yield call(subscribe_api, datas);
    if (response.data.status === 200) {
      if (response.data.error) {
        switch (response.data.error.code) {
          case "resource_missing":
            yield put({
              type: "SNACK_PUT_ERROR",
              message: "Veuillez enregistrer un moyen de paiement."
            });
            break;
        }
      } else {
        yield put({
          type: "SNACK_PUT_SUCCESS",
          message: "Vous avez un nouvel abonnement !",
          pathToRedirect: "/abonnements",
          redirect: true
        });
        yield put({
          type: "CLOSE_TUTORIAL"
        });
      }
    } else {
      yield put({
        type: "SNACK_PUT_ERROR",
        message: "Une erreur est survenue."
      });
    }
    yield put({ type: "END_LOADING" });
  } catch (e) {
    yield put({ type: "END_LOADING" });
    if (e.response.status === 403){
      yield put({
        type: "SNACK_PUT_ERROR",
        message: "Vous n'avez pas les droits pour effectuer cette action."
      });
    }
  }
}
