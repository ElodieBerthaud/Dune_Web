import axios from "axios";
import '../config';

export function verify_password(datas) {
  const datasTosend = new FormData();

  datasTosend.append("password", datas.password);

  return axios({
    method: "post",
    url: `${api_url_dev}/facturation/secure/verifPassword`,
    headers: {
      token: datas.token
    },
    data: datasTosend
  });
}

export async function send_payment_method(datas) {
  return await datas.cardElement.createPaymentMethod("card");
}

export function sendToken(datas) {
  return axios({
    method: "post",
    url: `${api_url_dev}/stripe/payments/addMethod`,
    headers: {
      token: datas.tokenSession
    },
    data: { pm_id: datas.pm_id }
  });
}

export function checkoutSession(datas) {
  return axios({
    method: "post",
    url: `${api_url_dev}/stripe/payments/buyAppCheckout`,
    headers: {
      token: datas.token
    },
    data: { items: datas.items, idApp: datas.idApp }
  });
}

export function get_invoices_api(datas) {
  return axios({
    method: "get",
    url: `${api_url_dev}/facturation/getFactures`,
    headers: {
      token: datas.token
    }
  });
}
