import axios from "axios";
import '../config';

// Get all classes of a professor.
export function get_user_classes_api(datas) {
  return axios({
    method: "get",
    url: `${api_url_dev}/trombi/classes`,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      token: datas.token
    }
  });
}
