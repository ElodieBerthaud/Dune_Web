import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { connect } from "react-redux";

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: "#ff5252",
    color: theme.palette.common.white
  },
  body: {
    fontSize: 14
  }
}))(TableCell);

const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3),
    overflowX: "auto"
  },
  table: {
    minWidth: 700
  },
  row: {
    "&:nth-of-type(odd)": {
      backgroundColor: "#ffebee"
    }
  }
});

function FactureList(props) {
  const { classes, get_invoices, token, invoices } = props;

  // Similaire à componentDidMount et componentDidUpdate :
  useEffect(() => {
    get_invoices(token);
  }, []);

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <CustomTableCell>Libelle</CustomTableCell>
            <CustomTableCell align="right">Type</CustomTableCell>
            <CustomTableCell align="right">Date</CustomTableCell>
            <CustomTableCell align="right">Montant TTC</CustomTableCell>
            <CustomTableCell align="right">Montant HT</CustomTableCell>
            <CustomTableCell align="right">Statut</CustomTableCell>
            <CustomTableCell align="right">Détails</CustomTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {invoices != null
            ? invoices.map(invoice => (
                <TableRow className={classes.row} key={invoice.id}>
                  <CustomTableCell component="th" scope="row">
                    {invoice.type + " " + invoice.name}
                  </CustomTableCell>
                  <CustomTableCell align="right">
                    {invoice.type}
                  </CustomTableCell>
                  <CustomTableCell align="right">
                    {invoice.date}
                  </CustomTableCell>
                  <CustomTableCell align="right">
                    {invoice.montant_ht}
                  </CustomTableCell>
                  <CustomTableCell align="right">
                    {invoice.montant_ttc}
                  </CustomTableCell>
                  <CustomTableCell align="right">
                    {invoice.status}
                  </CustomTableCell>
                  <CustomTableCell align="right">
                    {invoice.detail}
                  </CustomTableCell>
                </TableRow>
              ))
            : null}
          {invoices != null && invoices.length === 0 ? (
            <TableRow>
              <TableCell colSpan={6}>
                Aucune facture n'a été retrouvée sur votre compte.
              </TableCell>
            </TableRow>
          ) : null}
        </TableBody>
      </Table>
    </Paper>
  );
}

FactureList.propTypes = {
  classes: PropTypes.object.isRequired,
  get_invoices: PropTypes.func,
  token: PropTypes.string,
  invoices: PropTypes.array
};

const mapStateToProps = state => ({
  invoices: state.getInvoices.invoices,
  token: state.login.token
});

const mapDispatchToProps = dispatch => ({
  get_invoices: token =>
    dispatch({
      type: "GET_INVOICES",
      token
    })
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(FactureList));
