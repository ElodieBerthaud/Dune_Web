import React, { Component } from "react";
import { withStyles } from "@material-ui/styles";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import PropTypes from "prop-types";

class CreateCard extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return <div>Comp</div>;
  }
}

CreateCard.propTypes = {
  classes: PropTypes.object.isRequired
};


export default withRouter(
  connect(
    null,
    null
  )(withStyles(null)(CreateCard))
);
