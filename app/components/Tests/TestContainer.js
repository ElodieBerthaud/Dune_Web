import React, { Component } from "react";
import { Elements } from "react-stripe-elements";
import PaymentForm from "../Payments/CardForm";

class Follow extends Component {
  render() {
    return (
      <div>
        <Elements>
          <PaymentForm />
        </Elements>
      </div>
    );
  }
}

export default Follow;
