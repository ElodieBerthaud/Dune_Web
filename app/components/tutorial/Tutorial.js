import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/styles";

// dialog
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";

// stepper
import StepperTutorial from "./StepperTutorial";

const style = {
  title: {
    textAlign: "center"
  }
};

class Tutorial extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div>
        <Dialog
          open={this.props.tutorial}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          fullWidth
          maxWidth="xl"
        >
          <DialogTitle id="alert-dialog-title" style={{ textAlign: "center" }}>
            Bienvenue sur votre espace Dune !
          </DialogTitle>
          <StepperTutorial />
        </Dialog>
      </div>
    );
  }
}

Tutorial.propTypes = {
  classes: PropTypes.object
};

const mapStateToProps = state => ({
  tutorial: state.login.tutorial
});

export default connect(
  mapStateToProps,
  null
)(withStyles(style)(Tutorial));
