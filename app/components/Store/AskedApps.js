import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/styles";
import Notificationreport from "../Notifications/NotificationReport";

const styles = {
  card: {},
  media: {
    height: 50
  }
};

let id = 0;
function createData(name, calories, fat, carbs, protein) {
  id += 1;
  return {
    id,
    name,
    calories,
    fat,
    carbs,
    protein
  };
}

class AskedApps extends Component {
  render() {
    return (
      <div>
        <Notificationreport style={{ margin: "0 auto" }} size={12} />
      </div>
    );
  }
}

AskedApps.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AskedApps);
