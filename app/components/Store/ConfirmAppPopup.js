import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { withRouter } from "react-router";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";

class ConfirmAppPopup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      openAskApp: false,
      commentaire: ""
    };
  }

  handleClose = () => {
    this.setState({ openAskApp: false });
  };

  handleChangeComment = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleBuyAppDir = (free = "default") => {
    let demande = this.props.demande ? this.props.demande : null;
    let notif = this.props.notif ? this.props.notif : null;
    if (this.props.apps_left > 0 && free === "free") {
      this.props.buyAppDir(this.props.id, this.props.token, demande, notif);
    } else {
      this.props.startCheckout(
        this.props.token,
        this.props.items,
        this.props.id,
        demande,
        notif
      );
    }
  };

  handleConfirmAsk = () => {
    this.props.AskApp(
      this.props.id,
      this.props.token,
      this.props.idProf,
      this.props.idEcole,
      this.state.commentaire
    );
  };

  render() {
    return (
      <Dialog
        open={this.props.openAskApp}
        onClose={this.handleClose}
        aria-labelledby="form-dialog-title"
      >
        <div style={this.state.loader ? { display: "none" } : { display: "" }}>
          <DialogTitle id="form-dialog-title">
            Demande d'application
          </DialogTitle>
          <DialogContent>
            {this.props.typeUser !== 2
              ? "Vous êtes sur le point de faire une demande d'achat d'application a votre directeur d'etablissement. " +
                "Cette application ne sera disponible que si" +
                "le directeur l'accepte. Etes-vous sur de vouloir continuer ?"
              : "Vous etres sur le point d'acheter une application. Si vous continuez, vous serez facturé de " +
                this.props.prix +
                " euros."}

            {this.props.apps_left > 0
              ? "Si vous choisissez la reduction comprise dans votre abonnement, vous ne paierai pas cette application et l'aurez directement dans votre bibliothèque en cliquant sur obtenir gratuitement"
              : ""}

            {this.props.typeUser !== 2 ? (
              <TextField
                label="Votre commentaire"
                multiline
                rowsMax="4"
                value={this.state.commentaire}
                onChange={this.handleChangeComment}
                margin="normal"
                variant="outlined"
                name="commentaire"
                fullWidth
                rows={6}
              />
            ) : (
              ""
            )}
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Annuler
            </Button>
            <Button
              onClick={
                this.props.typeUser === 2
                  ? this.handleBuyAppDir
                  : this.handleConfirmAsk
              }
              color="primary"
            >
              Oui
            </Button>
            {this.props.typeUser === 2 && this.props.apps_left > 0 ? (
              <Button
                onClick={() => this.handleBuyAppDir("free")}
                color="primary"
              >
                Obtenir gratuitement
              </Button>
            ) : (
              ""
            )}
          </DialogActions>
        </div>
      </Dialog>
    );
  }
}

ConfirmAppPopup.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  return {
    token: state.login.token,
    app: state.appPage.appContent,
    idEcole: state.login.idEcole,
    appStatus: state.appPage.status,
    idProf: state.login.id_user,
    typeUser: state.login.typeUser,
    moyenne: state.nbAvis.moyenne,
    nbAvis: state.getAvis.nbAvis,
    apps_left: state.appPage.apps_left,
    app_competences: state.appPage.app_competences
  };
};

const mapDispatchToProps = dispatch => ({
  buyAppDir: (idApp, token, demande, notif) =>
    dispatch({ type: "BUY_APP_REQUEST", idApp, token, demande, notif }),
  startCheckout: (token, items, idApp, demande, notif) =>
    dispatch({ type: "START_CHECKOUT", token, items, idApp, demande, notif }),
  AskApp: (idApp, token, idProf, idEcole, commentaire) =>
    dispatch({
      type: "ASK_APP_REQUEST",
      idApp,
      token,
      idProf,
      idEcole,
      commentaire
    })
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withStyles(null)(ConfirmAppPopup))
);
