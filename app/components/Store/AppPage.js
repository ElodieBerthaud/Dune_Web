import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import PutAvis from "../Avis/PutAvis";
import GetAvis from "../Avis/getAvis";
import dashboardStyle from "../Dash/Dashboard/styles/dashboardStyle";
import Rating from "@material-ui/lab/Rating";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import Collapse from "@material-ui/core/Collapse";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ConfirmApp from "./ConfirmAppPopup";
import "./store.css";

const styles = theme => ({
  card: {
    minWidth: 275
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)"
  },
  pos: {
    marginBottom: 12
  },
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper
  },
  gridList: {
    flexWrap: "nowrap",
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: "translateZ(0)"
  },
  title: {
    color: theme.palette.primary.light
  },
  titleBar: {
    background:
      "linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)"
  }
});

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

class AppPage extends React.Component {
  emptyContent = true;

  state = {
    id: null,
    value: 0,
    nameApp: null,
    picPath: null,
    nomCreator: null,
    openAskApp: false,
    moyenne: null,
    commentaire: "",
    items: null,
    open: false
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { id } = this.props.match.params;

    this.state.id = id;

    this.props.getApp(this.state.id, this.props.token, this.props.idEcole);

    this.props.getNbAvis(this.state.id, this.props.token);
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeComment = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleGetApp = (event, value) => {
    if (this.props.appStatus === "0") {
      this.setState({
        items: {
          name: this.state.nameApp,
          description:
            this.props.app !== null ? this.props.app.description : "",
          amount: this.props.app !== null ? this.props.app.prix * 100 : "",
          currency: "eur",
          quantity: 1
        }
      });
      this.setState({ openAskApp: true });
    }
  };

  handleConfirmAsk = () => {
    this.props.AskApp(
      this.state.id,
      this.props.token,
      this.props.idProf,
      this.props.idEcole,
      this.state.commentaire
    );
  };

  handleBuyAppDir = (free = "default") => {
    if (this.props.apps_left > 0 && free === "free") {
      this.props.buyAppDir(this.state.id, this.props.token);
    } else {
      this.props.startCheckout(
        this.props.token,
        this.state.items,
        this.state.id
      );
    }
  };

  handleClick = () => {
    this.setState({ open: !this.state.open });
  };

  handleClose = () => {
    this.setState({ openAskApp: false });
  };

  render() {
    if (this.props.app != null) {
      this.state.nameApp = this.props.app.nomApp;
      this.state.picPath = this.props.app.picPath;
      this.state.nomCreator = this.props.app.nomCreator;
    }

    let comps = [];
    if (this.props.app_competences !== null) {
      comps = this.props.app_competences.map((item, key) => (
        <li key={item.idComp}>{item.libelleComp}</li>
      ));
    }
    const { classes } = this.props;
    return (
      <Card className={classes.card}>
        <CardContent>
          <Grid container spacing={4}>
            <Grid item xs={3}>
              <img
                style={{ width: "100%" }}
                src={
                  this.state.picPath !== null
                    ? `${api_url_dev}/files/apps/${this.state.picPath}`
                    : ""
                }
              />
            </Grid>
            <Grid item xs={3} style={{ lineHeight: "2.5" }}>
              {this.state.nameApp}
              <br />
              1.0.1
              <br />
              MATHEMATIQUES
              <br />
              {this.state.nomCreator}
              <br />
              {this.props.app !== null ? this.props.app.nb_joueurs : ""} joueurs
              <br />
              <Rating name="read-only" value={this.props.moyenne} readOnly /> (
              {this.props.nbAvis})
              <br />
            </Grid>
            <Grid item xs={3}>
              <Paper className={classes.paper}></Paper>
            </Grid>
            <Grid item xs={3} style={{ textAlign: "center" }}>
              <div style={{ fontWeight: "bold" }}>
                {this.props.app !== null ? this.props.app.prix + " euros" : ""}
                {this.props.app !== null ? (
                  this.props.apps_left > 0 ? (
                    <span style={{ color: "#C23D4B", fontWeight: "normal" }}>
                      {" "}
                      <br />
                      Avec votre abonnement, vous pouvez profiter de cette
                      application gratuitement.{" "}
                    </span>
                  ) : (
                    ""
                  )
                ) : (
                  ""
                )}
              </div>
              <Button
                onClick={this.handleGetApp}
                style={{ width: "50%", margin: "0 auto" }}
                variant="contained"
                color="primary" // <-- Just add me!
                label="My Label"
                className={classes.button}
              >
                <input
                  style={{ position: "absolute", opacity: "0" }}
                  disabled={this.props.appStatus !== "0"}
                />
                {this.props.appStatus === "0"
                  ? this.props.typeUser === 2
                    ? "ACHETER"
                    : "SOUMETTRE"
                  : "DEJA DANS LA BIBLIOTHEQUE"}
              </Button>
            </Grid>
            <Grid item xs={4}>
              <List>
                <ListItem button onClick={this.handleClick}>
                  <ListItemText primary="Compétences associées" />
                  {this.state.open ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={this.state.open} timeout="auto" unmountOnExit>
                  <ul>{comps}</ul>
                </Collapse>
              </List>
            </Grid>
            <Grid item xs={12}>
              {this.props.app !== null ? this.props.app.description : ""}
            </Grid>
            <Grid item xs={6}>
              <span style={{ fontWeight: "bold" }}>Developpeur</span>
              <br />
              {this.state.nomCreator}
            </Grid>
            <Grid item xs={6}>
              <span style={{ fontWeight: "bold" }}>Categorie</span>
              <br />
              Mathematiques.
            </Grid>
            <Grid item xs={6}>
              <span style={{ fontWeight: "bold" }}>Joueurs</span>
              <br />
              {this.props.app !== null ? this.props.app.nb_joueurs : ""} joueurs
            </Grid>
            <Grid item xs={6}>
              <span style={{ fontWeight: "bold" }}>Niveau</span>
              <br />
              CE1
            </Grid>
            <Grid item xs={12}>
              <Tabs
                value={this.state.value}
                onChange={this.handleChange}
                indicatorColor="primary"
                textColor="primary"
                centered
              >
                <Tab label={`${"Avis " + "("}${this.props.nbAvis})`} />
                <Tab label="Laisser un avis" />
              </Tabs>

              {this.state.value === 0 && (
                <TabContainer>
                  <GetAvis
                    empty={this.emptyContent}
                    contentAvis={this.props.contentAvis}
                    idApp={this.state.id}
                  />
                </TabContainer>
              )}
              {this.state.value === 1 && (
                <TabContainer>
                  <PutAvis idApp={this.state.id} />
                </TabContainer>
              )}
            </Grid>
          </Grid>
        </CardContent>
        <ConfirmApp
          openAskApp={this.state.openAskApp}
          id={this.props.app != null ? this.props.app.id : null}
          items={this.state.items}
          prix={this.props.app != null ? this.props.app.prix : ""}
          demande={null}
          notif={null}
        />
      </Card>
    );
  }
}

AppPage.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  token: state.login.token,
  app: state.appPage.appContent,
  idEcole: state.login.idEcole,
  appStatus: state.appPage.status,
  idProf: state.login.id_user,
  typeUser: state.login.typeUser,
  moyenne: state.nbAvis.moyenne,
  nbAvis: state.getAvis.nbAvis,
  apps_left: state.appPage.apps_left,
  app_competences: state.appPage.app_competences
});

const mapDispatchToProps = dispatch => ({
  getApp: (idApp, token, idEcole) =>
    dispatch({
      type: "GET_APP_REQUEST",
      idApp,
      token,
      idEcole
    }),
  AskApp: (idApp, token, idProf, idEcole, commentaire) =>
    dispatch({
      type: "ASK_APP_REQUEST",
      idApp,
      token,
      idProf,
      idEcole,
      commentaire
    }),
  buyAppDir: (idApp, token) =>
    dispatch({ type: "BUY_APP_REQUEST", idApp, token }),
  getAvis: (idGame, token) =>
    dispatch({ type: "GET_AVIS_REQUEST", idGame, token }),
  getNbAvis: (idGame, token) =>
    dispatch({ type: "GET_NB_AVIS_REQUEST", idGame, token }),
  startCheckout: (token, items, idApp) =>
    dispatch({ type: "START_CHECKOUT", token, items, idApp })
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withStyles(dashboardStyle)(AppPage))
);
