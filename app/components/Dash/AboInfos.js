//Basics
import React from "react";
import PropTypes from "prop-types";

//Ui-core
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

//Routing
import { withRouter } from "react-router";
import { connect } from "react-redux";

//Stylesheets
import { makeStyles } from "@material-ui/core/styles";
import "./dash.css";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%"
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: "33.33%",
    flexShrink: 0
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary
  }
}));

function ControlledExpansionPanels(props) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);
  const { current_abo_state } = props;
  const handleChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };
  let storage_left,
    apps_left,
    storage_total,
    apps_total = null;

  if (current_abo_state != null) {
    storage_left = current_abo_state.storage_left;
    apps_left = current_abo_state.apps_left;
    storage_total = current_abo_state.storage_total;
    apps_total = current_abo_state.apps_total;
  }
  return (
    <div className={classes.root}>
      <ExpansionPanel
        expanded={expanded === "panel1"}
        onChange={handleChange("panel1")}
      >
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1bh-content"
          id="panel1bh-header"
        >
          <Typography className={classes.secondaryHeading}>
            Détails de l'offre
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          {!(props.current_abo === null) ?
          <ul>
            <li>
              <Typography>
                {storage_left === 999999 ? "Vous avez stockage illimité avec votre offre." :
                    "Il vous reste" +
                  <span className="aboInfos">{storage_left } Go</span> + "d'espace de stockage sur" +
                  <span className="aboInfos">{storage_total} Go</span> + "."
                }
              </Typography>
            </li>
            <li>
              <Typography>
                Il vous reste <span className="aboInfos"> {apps_left} </span>
                  applications gratuites sur un total de {apps_total}
                  applications.
              </Typography>
            </li>
          </ul>
              : "Veuillez vous rendre sur l'onglet abonnement pour pouvoir profiter de nos offres. " +
              "Sans abonnement, vous ne pouvez pas profiter de licences d'activation de la table Dune."

          }
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </div>
  );
}

const mapStateToProps = state => ({
  current_abo_state: state.subscribe.current_abo_state,
  current_abo: state.subscribe.current_abo
});

ControlledExpansionPanels.propTypes = {
  storage: PropTypes.number,
  apps: PropTypes.number,
};

export default withRouter(
  connect(
    mapStateToProps,
    null
  )(ControlledExpansionPanels)
);
