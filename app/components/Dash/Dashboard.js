import { connect } from "react-redux";
import React from "react";
import { withRouter } from "react-router";

import PropTypes from "prop-types";
// @material-ui/icons
import DateRange from "@material-ui/icons/DateRange";
import Update from "@material-ui/icons/Update";
import Accessibility from "@material-ui/icons/Accessibility";
import Games from "@material-ui/icons/Games";
import Apps from "@material-ui/icons/Apps";
import Ballot from "@material-ui/icons/Ballot";
import CommentIcon from "@material-ui/icons/Visibility";

// @material-ui/core
import withStyles from "@material-ui/styles/withStyles";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import IconButton from "@material-ui/core/IconButton";
import dashboardStyle from "./Dashboard/styles/dashboardStyle.jsx";
import CardFooter from "./Dashboard/CardFooter.jsx";
import CardBody from "./Dashboard/CardBody.jsx";
import CardIcon from "./Dashboard/CardIcon.jsx";
import CardHeader from "./Dashboard/CardHeader.jsx";
import Card from "./Dashboard/Card.jsx";
import Table from "./Dashboard/Table.jsx";
import GridContainer from "./Dashboard/GridContainer.jsx";
import GridItem from "./Dashboard/GridItem.jsx";

import ShowNotif from "../Notifications/ShowNotif";
import Notificationreport from "../Notifications/NotificationReport";
import AboInfos from "./AboInfos";
import "./dash.css";
import Chart from "react-google-charts";

const options = {
  chart: {
    title: "Moyennes",
    subtitle: "Visualisation des moyenens de l'élève"
  },
  colors: ["#ff1744", "#7e57c2", "#7570b3"],
  vAxis: {
    viewWindowMode: "explicit",
    viewWindow: {
      max: 100,
      min: 0
    }
  },
  scaleShowGridLines: true,
  scaleGridLineColor: "rgba(0,0,0,.05)",
  scaleGridLineWidth: 1,
  scaleShowHorizontalLines: true,
  scaleShowVerticalLines: true,
  bezierCurve: true,
  bezierCurveTension: 0.4,
  pointDot: true,
  pointDotRadius: 4,
  pointDotStrokeWidth: 1,
  pointHitDetectionRadius: 20,
  datasetStroke: true,
  datasetStrokeWidth: 2,
  datasetFill: true,
  globalResponsive: true
};

class Dashboard extends React.Component {
  state = {
    value: 0
  };

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      openNotif: false,
      appsAsked: [],
      expanded: false,
      data: []
    };

    this.classesLabel = {
      1: "Petite section",
      2: "Moyenne Section",
      3: "Grande section",
      4: "CP",
      5: "CE1",
      6: "CE2",
      7: "CM1",
      8: "CM2",
      9: "6e",
      10: "5e",
      11: "4e",
      12: "3e"
    };

    this.generateAllNotif = this.generateAllNotif.bind(this);
  }

  componentWillMount() {
    const {
      getStudentsNbr,
      getAppsNbr,
      token,
      updateDash,
      SubInfos
    } = this.props;

    getStudentsNbr(token);
    getAppsNbr(token);
    updateDash(token);
    SubInfos(token);
  }

  showNotif(idNotif) {
    this.setState({ openNotif: true });

    this.props.showNotif(idNotif, this.props.token);
  }

  generateAllNotif = () => {
    const obj = this.props.content;

    const notifs = [];

    let id = null;

    if (obj != null) {
      for (let i = 0; i < this.props.nbNotifs; i++) {
        id = obj[i].idNotif;

        if (obj[i].isRead === 0) {
          notifs.push(
            <ListItem key={i} role={undefined} dense button>
              <ListItemText primary={obj[i].textNotif} />
              <ListItemSecondaryAction>
                <IconButton
                  onClick={this.showNotif.bind(this, id)}
                  aria-label="Comments"
                >
                  <CommentIcon />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          );
        }
      }
    }
    return notifs;
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    return true;
  }

  renderRankTab = () => {
    if (this.props.studentRank !== null && this.props.studentRank.length > 0) {
      const id = null;
      const src = null;
      let ranks = [];

      ranks = this.props.studentRank.map((rank, i) => [
        `${rank.nomEleve.toString()} ${rank.prenomEleve.toString()}`,
        (Math.round(rank.score * 100) / 100).toString(),
        (i + 1).toString()
      ]);
      return ranks;
    }
    return [[]];
  };

  handleCloseDashNotifs = () => {
    this.setState({ anchorEl: null, dashBoardNotif: false });
  };

  handleCloseDashNotifsStop = () => {
    this.setState({ anchorEl: null, dashBoardNotif: false });
    this.props.stopNotif();
  };

  createChartArray = () => {
    const tmp = [];

    this.state.data = [];
    /*if (this.state.data !== []){
      this.setState({data: []});
    }*/
    this.state.data.push(["Classe", "Moyenne"]);

    if (this.props.contentAvg !== null) {
      for (let i = 0; i < this.props.contentAvg.length; i++) {
        const tmp = [];

        tmp.push(
            `${this.classesLabel[this.props.contentAvg[i].level]}-${
                this.props.contentAvg[i].num
                }`
        );
        tmp.push(parseInt(this.props.contentAvg[i].moyenne));

        this.state.data.push(tmp);
      }
    }
  };

  render() {
    const { classes } = this.props;
    this.createChartArray();
    return (
      <div>
        <div style={{ marginBottom: "2%" }}>
          <h2 style={{ textAlign: "center", color: "grey" }}>
            Bonjour {this.props.name} {this.props.lastname}, que voulez-vous
            faire aujourd'hui ?
          </h2>
        </div>
        <div>
          <GridContainer>
            <GridItem xs={12} sm={6} md={3}>
              <Card>
                <CardHeader color="info" stats icon>
                  <CardIcon color="info">
                    <Accessibility />
                  </CardIcon>
                  <p className={classes.cardCategory}>Nombres d'éleves</p>
                  <h3 className={classes.cardTitle}>{this.props.nbStudents}</h3>
                </CardHeader>
                <CardFooter stats>
                  <div>
                    <Update />
                    <a href="/students">Accéder au trombinoscope</a>
                  </div>
                </CardFooter>
              </Card>
            </GridItem>
            <GridItem xs={12} sm={6} md={3}>
              <Card>
                <CardHeader color="primary" stats icon>
                  <CardIcon color="primary">
                    <Apps />
                  </CardIcon>
                  <p className={classes.cardCategory}>Applications / Jeux</p>
                  <h3 className={classes.cardTitle}>{this.props.nbGames}</h3>
                </CardHeader>
                <CardFooter stats>
                  <div>
                    <DateRange />
                    <a href="/store">Voir votre bibliothèque</a>
                  </div>
                </CardFooter>
              </Card>
            </GridItem>
            <GridItem xs={12} sm={6} md={3}>
              <Card>
                <CardHeader color="success" stats icon>
                  <CardIcon color="success">
                    <Games />
                  </CardIcon>
                  <p className={classes.cardCategory}>Parties jouées</p>
                  <h3 className={classes.cardTitle}>{this.props.gamesNbr}</h3>
                </CardHeader>
                <CardFooter stats>
                  <div>
                    <DateRange />
                    Ce mois-ci
                  </div>
                </CardFooter>
              </Card>
            </GridItem>
            <GridItem xs={12} sm={6} md={3}>
              <Card>
                <CardHeader color={"danger"} stats icon>
                  <CardIcon color={"danger"}>
                    <Ballot />
                  </CardIcon>
                  <p className={classes.cardCategory}>Votre offre actuelle</p>
                  <h3 className={classes.cardTitle}>
                    {this.props.current_abo === 1 ? "Basique" : (this.props.current_abo === 2 ? "Premium" : "Aucune offre")}
                  </h3>
                </CardHeader>
                <CardFooter stats>
                  <AboInfos />
                </CardFooter>
              </Card>
            </GridItem>
          </GridContainer>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <Card chart>
                <CardHeader color="warning">
                  <Chart
                      loader={"../images/loaders/bars-loader.gif"}
                      redraw
                      options={options}
                      chartType="ColumnChart"
                      width="100%"
                      height="400px"
                      data={this.state.data}
                  />
                </CardHeader>
                <CardBody>
                  <h4 className={classes.cardTitle}>
                    Moyenne globales de vos classes
                  </h4>
                  <p className={classes.cardCategory}/>
                </CardBody>
              </Card>
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <Card>
                <CardHeader color="warning">
                  <h4 className={classes.cardTitleWhite}>
                    Classement global de vos éleves
                  </h4>
                  <p className={classes.cardCategoryWhite}>
                    Classement de vos eleves pour ce trimestre.
                  </p>
                </CardHeader>
                <CardBody>
                  <Table
                    tableHeaderColor="warning"
                    tableHead={["Nom", "Score", "Rang"]}
                    tableData={this.renderRankTab()}
                  />
                </CardBody>
              </Card>
            </GridItem>
          </GridContainer>
          <GridContainer>
            <Notificationreport size={6} />
          </GridContainer>
        </div>

        <Dialog
          open={
            this.props.dashBoardNotif &&
            this.props.showDash &&
            this.props.nbNotifs > 0
          }
          onClose={this.handleCloseDashNotifs}
          aria-labelledby="scroll-dialog-title"
        >
          <DialogTitle id="scroll-dialog-title" style={{ textAlign: "center" }}>
            Vous avez des notifications
          </DialogTitle>
          <DialogContent>
            <List>{this.generateAllNotif()}</List>
          </DialogContent>
          <DialogActions>
            <Button color="primary" onClick={this.props.stopNotifTmp}>
              Ok
            </Button>
            <Button color="primary" onClick={this.handleCloseDashNotifsStop}>
              Ne plus me prévenir
            </Button>
          </DialogActions>
        </Dialog>

        <ShowNotif />
      </div>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  name: state.user.name,
  lastname: state.user.lastname,
  isDirector: state.login.director,
  token: state.login.token,
  nbStudents: state.students.nbStudents,
  typeUser: state.login.typeUser,
  idUser: state.login.id_user,
  typeNotif: state.showNotif.typeNotif,
  nbNotifs: state.notification.nbNotif,
  content: state.notification.content,
  showDash: state.showDash.showDash,
  nbGames: state.appRegistred.appsNbr,
  contentProf: state.showNotif.contentProf,
  dashBoardNotif: state.showDashTmp.showDash,
  gamesNbr: state.dashboard.GamesPlayed,
  studentRank: state.dashboard.rank,
  contentAvg: state.dashboard.classesAvg,
  current_abo: state.subscribe.current_abo
});

const mapDispatchToProps = dispatch => ({
  showNotif: (idNotif, token) =>
    dispatch({ type: "SHOW_NOTIF_REQUEST", idNotif, token }),
  getStudentsNbr: token => dispatch({ type: "GET_STUDENTSNBR_REQUEST", token }),
  getAppsNbr: token => dispatch({ type: "GET_APPNBR_REQUEST", token }),
  stopNotif: () => dispatch({ type: "NOTIFS_DASH_STOP" }),
  stopNotifTmp: () => dispatch({ type: "SHOW_DASH_TMP_STOP" }),
  updateDash: token => dispatch({ type: "GET_DASHBOARD_REQUEST", token }),
  SubInfos: token => dispatch({ type: "GET_SUB_INFO_REQUEST", token })
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withStyles(dashboardStyle)(Dashboard))
);
