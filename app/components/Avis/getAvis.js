import React, { Component } from "react";
import withStyles from "@material-ui/styles/withStyles";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import List from "@material-ui/core/List";
import avatar from "../../images/avatar.png";

import Typography from "@material-ui/core/Typography";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
import Rating from "@material-ui/lab/Rating";

const styles = theme => ({});

function getDate(timestamp) {
  const ts = new Date(timestamp);

  return ts.toLocaleString([], {
    day: "2-digit",
    month: "2-digit",
    year: "numeric",
    hour: "2-digit",
    minute: "2-digit"
  });
}

class getAvis extends Component {
  constructor(props) {
    super(props);

    // this.lastContent = [];

    this.state = {
      commentaire: "",
      id: null,
      loadMore: false,
      render: false,
      lastContent: []
    };
  }

  componentDidMount() {
    this.setState({ lastContent: [] });

    const { id } = this.props.match.params;

    this.state.id = id;

    this.props.getAvis(id, this.props.token, 0);
  }

  componentWillUnmount() {
    this.setState({ lastContent: [] });

    this.state.lastContent = [];
  }

  getAvis = () => {
    const { id } = this.props.match.params;

    this.state.id = id;

    this.props.getAvis(id, this.props.token, this.props.lastNbAvis);

    this.state.loadMore = true;
  };

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    if (nextProps.avis !== this.props.avis) {
      return true;
    }
    return false;
  }

  renderAvisList = () => {
    const contentToReturn = [];

    if (this.props.avis !== null) {
      if (this.props.avis.length === 0) {
        contentToReturn.push(
          <div style={{ textAlign: "center" }} key={0}>
            Soyez le premier à laisser un avis !
          </div>
        );
      } else {
        for (let i = 0; i < this.props.avis.length; i++) {
          this.state.lastContent.push(
            <div key={i}>
              <ListItem alignitems="flex-start" key={this.props.avis[i].id}>
                <ListItemAvatar>
                  <Avatar
                    alt=""
                    src={
                      this.props.avis[i].photo == "null"
                        ? `https://api.dune-table.com/files/profs/${this.props.avis[i].photo}`
                        : avatar
                    }
                  />
                </ListItemAvatar>
                <ListItemText
                  primary={`${this.props.avis[i].nomProf} ${this.props.avis[i].prenomProf}`}
                  secondary={
                    <React.Fragment>
                      <Typography component="span" color="textPrimary">
                        {getDate(this.props.avis[i].date)}
                      </Typography>
                      <br />
                      <Typography component="span" color="textPrimary">
                        <Rating
                          name="read-only"
                          value={this.props.avis[i].note}
                          readOnly
                        />
                      </Typography>
                      <br />
                      <Typography
                        component="span"
                        color="textPrimary"
                        style={{ fontStyle: "italic" }}
                      >
                        {this.props.avis[i].commentaire}
                      </Typography>
                    </React.Fragment>
                  }
                />
              </ListItem>
              <Divider variant="inset" />
            </div>
          );
        }

        contentToReturn.push(this.state.lastContent);

        if (this.state.lastContent.length < this.props.nbAvis) {
          contentToReturn.push(
            <div style={{ textAlign: "center", margin: "5%" }} key={-1}>
              <Button
                variant="contained"
                color="primary"
                onClick={this.getAvis}
              >
                Charger plus d'avis
              </Button>
            </div>
          );
        }
      }
    }

    return contentToReturn;
  };

  render() {
    return (
      <div>
        <List id="renderAvisList" key={0}>
          {this.renderAvisList()}
        </List>
      </div>
    );
  }
}

getAvis.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  avis: state.getAvis.contentAvis,
  token: state.login.token,
  nbAvis: state.getAvis.nbAvis,
  lastNbAvis: state.nbAvis.lastNbAvis
});

const mapDispatchToProps = dispatch => ({
  getAvis: (idGame, token, depart) =>
    dispatch({
      type: "GET_AVIS_REQUEST",
      idGame,
      token,
      depart
    })
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withStyles(styles)(getAvis))
);
