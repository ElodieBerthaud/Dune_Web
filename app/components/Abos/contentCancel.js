import React, { Component } from "react";
import withStyles from "@material-ui/styles/withStyles";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";

const style = {};

class contentCancel extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div>
        <DialogTitle id="alert-dialog-title">
          Mettre fin à votre abonnement {this.props.title}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
              Pour mettre fin à votre abonnement, veuillez s'il vous plaît contacter l'équipe du support par
              {' '}<a href="mailto:dune.epitech.contact@gmail.com" style={{textDecoration: 'underline'}}>mail</a> ou via
              {' '}<a href="/contact" style={{textDecoration: 'underline'}}>notre formulaire de contact</a>.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.ClosePopup} color="primary">
            Ok
          </Button>
        </DialogActions>
      </div>
    );
  }
}

contentCancel.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  ClosePopup: () => dispatch({ type: "ABO_POPUP_CLOSE" })
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withStyles(style)(contentCancel))
);
