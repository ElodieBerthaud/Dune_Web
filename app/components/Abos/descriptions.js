import React from "react";

export const abo1 = (
  <ul style={{ listStyleType: "none", padding: "0", margin: "0" }}>
    <li>10 Go de fichiers</li>
    <li>15 Applications gratuites</li>
  </ul>
);
export const abo2 = (
  <ul style={{ listStyleType: "none", padding: "0", margin: "0" }}>
    <li>Fichiers illimités</li>
    <li>30 Applications gratuites</li>
  </ul>
);
