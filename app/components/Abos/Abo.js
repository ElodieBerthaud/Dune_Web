import React, { Component } from "react";
import withStyles from "@material-ui/styles/withStyles";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import PropTypes from "prop-types";
import { abo1, abo2 } from "./descriptions";
import AboCard from "./AboCard";
import PopUp from "./popUp";

const style = theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  card: {
    minWidth: 275,
    textAlign: "center"
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)"
  },
  title: {
    // fontSize: 14,
    textAlign: "center"
  },
  pos: {
    marginBottom: 12
  },
  grid: {},
  center: {
    textAlign: "center"
  },
  cardHeader: {
    backgroundColor: "#e0f7fa"
  },
  price: {
    color: "#00838f"
  },
  bold: {
    fontWeight: "bold"
  }
});

class Abo extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {
    this.props.SubInfos(this.props.token);
  }

  render() {
    const classes = this.props.classes;
    return (
      <div>
        <div className={classes.root}>
          <Grid container spacing={4}>
            <Grid item md={6} xs={12} className={classes.grid}>
              <AboCard
                name="Basique"
                price={
                  <div>
                    <p className={classes.price}> 1800 € par table / an</p>
                  </div>
                }
                priceYear={1800}
                id={1}
                desc={abo1}
                isActive={
                  this.props.current_abo === 1 && this.props.status === 1
                }
                isNotActive={
                  this.props.current_abo === 1 && this.props.status === 0
                }
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <AboCard
                priceYear={2200}
                name="Premium"
                price={
                  <div>
                    <p className={classes.price}> 2200 € par table / an</p>
                  </div>
                }
                id={2}
                desc={abo2}
                isActive={
                  this.props.current_abo === 2 && this.props.status === 1
                }
                isNotActive={
                  this.props.current_abo === 2 && this.props.status === 0
                }
              />
            </Grid>
          </Grid>
        </div>
        <PopUp />
      </div>
    );
  }
}

Abo.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  current_abo: state.subscribe.current_abo,
  status: state.subscribe.isValid,
  token: state.login.token
});

const mapDispatchToProps = dispatch => ({
  SubInfos: token => dispatch({ type: "GET_SUB_INFO_REQUEST", token })
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withStyles(style)(Abo))
);
