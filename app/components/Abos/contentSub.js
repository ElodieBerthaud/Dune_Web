import React, { Component } from "react";
import withStyles from "@material-ui/styles/withStyles";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import "./abos.css";

const styles = theme => ({
  root: {
    display: "flex"
  },
  formControl: {
    margin: theme.spacing(3)
  },
  group: {
    margin: `${theme.spacing()}px 0`
  }
});
class contentSub extends Component {
  constructor(props) {
    super(props);

    this.state = {
      plan: 0,
      tables: 1,
      price: 0,
      remise: ""
    };
  }

  initialPrice() {
    switch (this.props.id) {
      case 1:
        return 1800;
      case 2:
        return 2200;
    }
  }

  handleChange = event => {
    let quotient = 0;
    // On calcule le quotient de réduction par rapport au nombre de table commandées par l'utilisateur
    if (event.target.value < 5 && event.target.value > 1) {
      quotient = 5;
    } else if (event.target.value >= 5) {
      quotient = 10;
    }
    let price = this.initialPrice();
    switch (event.target.name) {
      case "tables":
        const montant_remise = (price * event.target.value * quotient) / 100;
        this.setState({ price: price * event.target.value - montant_remise });
        this.setState({ [event.target.name]: event.target.value });
        quotient > 0
          ? this.setState({ remise: " dont une remise de " + quotient + "%" })
          : this.setState({ remise: "" });
        break;
      default:
        this.setState({ [event.target.name]: event.target.value });
        break;
    }
  };

  componentDidMount() {
    this.setState({ price: this.initialPrice() });
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <DialogTitle id="alert-dialog-title">
          Souscrire à l'abonnement {this.props.title}
        </DialogTitle>
        <DialogContent className="justify">
          <DialogContentText id="alert-dialog-description">
            Vous êtes sur le point de choisir l'abonnement
            {" " + this.props.title} <br />
            Veuillez entrer le nombre de table comprises dans cet abonnement
          </DialogContentText>

          <FormControl className={classes.formControl} className="nbTables">
            <InputLabel htmlFor="outlined-age-native-simple">
              Nombre de tables
            </InputLabel>
            <Select
              required
              native
              value={this.state.tables}
              onChange={this.handleChange}
              inputProps={{
                name: "tables",
                id: "outlined-age-native-simple"
              }}
            >
              <option value={1}>1</option>
              <option value={2}>2</option>
              <option value={3}>3</option>
              <option value={4}>4</option>
              <option value={5}>5</option>
              <option value={6}>6</option>
              <option value={7}>7</option>
              <option value={8}>8</option>
              <option value={9}>9</option>
              <option value={10}>10</option>
            </Select>
          </FormControl>
          <br />
          <DialogContentText id="alert-dialog-description">
            En souscrivant a cet abonnement, vous allez être débité de la somme
            de
            <span style={{ fontWeight: "bold", color: "#8bc34a" }}>
              {" " + this.state.price + " euros " + this.state.remise + " "}
            </span>
            sur le moyen de paiement que vous avez enregistré
          </DialogContentText>
          <DialogContentText style={{ color: "#c23d4b" }}>
            En cliquant sur CONTINUER, vous serez débité sur la carte que vous
            avez enregistré précédement. Vous recevrez un reçu par mail.
          </DialogContentText>
        </DialogContent>

        <DialogActions>
          <Button onClick={this.props.ClosePopup} color="primary">
            Annuler
          </Button>
          <Button
            onClick={() =>
              this.props.SubscribeAbo(
                this.props.id,
                this.state.tables,
                this.props.token
              )
            }
            color="primary"
            autoFocus
          >
            Continuer
          </Button>
        </DialogActions>
      </div>
    );
  }
}

contentSub.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  priceYear: state.subscribe.priceYear,
  priceMonth: state.subscribe.priceMonth,
  token: state.login.token
});

const mapDispatchToProps = dispatch => ({
  ClosePopup: () => dispatch({ type: "ABO_POPUP_CLOSE" }),
  SubscribeAbo: (plan, quantity, token) =>
    dispatch({ type: "SUBSCRIBE_ABO_REQUEST", plan, quantity, token })
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withStyles(styles)(contentSub))
);
